const path = require('path')
const fs = require('fs')
const Grid = require('gridfs-stream')
const mongoose = require('./src/lib/mongoose')
const app = require('./src/app')
const Users = require('./src/modules/users/users.model')
const Soundbox = require('./src/modules/soundbox/soundbox.model')

async function start() {
	await app.start(9001)

	const gfs = Grid(mongoose.connection.db, mongoose.mongo)

	await mongoose.connection.db.dropDatabase()

	const pw = await Users.password('nicu')
	const pw2 = await Users.password('nicu2')
	const pw3 = await Users.password('nicu3')
	const pw4 = await Users.password('nicu4')

	const user = await Users.create({
		email: 'ima@user.com',
		password: pw,
		username: 'ImAUser',
		bio: 'I am the most famous user on the planet',
		roles: [
			'user'
		]
	})

	const user2 = await Users.create({
		email: 'lil@Y.com',
		password: pw2,
		username: 'lil-Y',
		bio: 'I am Lil-Y, rap is my everything <3',
		roles: [
			'user'
		]
	})

	const user3 = await Users.create({
		email: 'weeb@aboo.com',
		password: pw3,
		username: 'WeebAboo',
		bio: 'I-its not like I want you to listen to my songs... b-baka!',
		roles: [
			'user'
		]
	})

	const user4 = await Users.create({
		email: 'wut@wat.com',
		password: pw4,
		username: 'WutWat',
		bio: 'I say wut wat... in the wutt',
		roles: [
			'user'
		]
	})

	let writestream = gfs.createWriteStream({
		filename: 'song.mp3',
		mode: 'w',
		content_type: 'audio/mpeg'
	})

	const result = await fs.createReadStream(path.join(__dirname, '/src/assets/files/sourcefile.mp3')).pipe(writestream)

	writestream = gfs.createWriteStream({
		filename: 'song2.mp3',
		mode: 'w',
		content_type: 'audio/mpeg'
	})

	const result2 = await fs.createReadStream(path.join(__dirname, '/src/assets/files/sourcefile.mp3')).pipe(writestream)

	writestream = gfs.createWriteStream({
		filename: 'song3.mp3',
		mode: 'w',
		content_type: 'audio/mpeg'
	})

	const result3 = await fs.createReadStream(path.join(__dirname, '/src/assets/files/sourcefile.mp3')).pipe(writestream)

	writestream = gfs.createWriteStream({
		filename: 'song4.mp3',
		mode: 'w',
		content_type: 'audio/mpeg'
	})

	const result4 = await fs.createReadStream(path.join(__dirname, '/src/assets/files/sourcefile.mp3')).pipe(writestream)

	await Soundbox.create({
		user: user._id,
		title: 'Fuckboiiiii song',
		description: 'This is a song by a fuckboi for fuckbois',
		collaborators: [
			user2._id,
			user4._id
		],
		soundfiles: [
			{
				title: 'sourcefile',
				author: user._id,
				file: result.id,
				category: 'vocals',
				rating: []
			},
			{
				title: 'sourcefile',
				author: user2._id,
				file: result3.id,
				category: 'beats',
				rating: []
			}
		],
		releases: []
	})

	await Soundbox.create({
		user: user2._id,
		title: 'Rap goddess ft. Lil-Y',
		description: 'This song is by the most famous rapper Lil-Y',
		collaborators: [
			user._id,
			user3._id
		],
		soundfiles: [
			{
				title: 'sourcefile',
				author: user2._id,
				file: result2.id,
				category: 'mix',
				rating: []
			},
			{
				title: 'sourcefile',
				author: user._id,
				file: result4.id,
				category: 'instrumental',
				rating: []
			}
		],
		releases: []
	})

	await Soundbox.create({
		user: user3._id,
		title: 'Cruel Pokemons Thesis',
		description: `zaaaankoku na tenshi no you ni
		shoooooooounen yoooo shinwa ni nareeeeeeee!!!!!!`.toUpperCase(),
		collaborators: [
			user._id,
			user2._id
		],
		soundfiles: [
			{
				title: 'sourcefile',
				author: user3._id,
				file: result3.id,
				category: 'vocals',
				rating: []
			},
			{
				title: 'sourcefile',
				author: user._id,
				file: result4.id,
				category: 'mix',
				rating: []
			}
		],
		releases: []
	})

	await Soundbox.create({
		user: user4._id,
		title: 'Wut wut wat wat',
		description: 'Seriously wut wut wat wat',
		collaborators: [
			user2._id,
			user3._id
		],
		soundfiles: [
			{
				title: 'sourcefile',
				author: user4._id,
				file: result4.id,
				category: 'beats',
				rating: []
			},
			{
				title: 'sourcefile',
				author: user2._id,
				file: result.id,
				category: 'instrumental',
				rating: []
			}
		],
		releases: []
	})

	console.log('Refilled database')
}

start()
