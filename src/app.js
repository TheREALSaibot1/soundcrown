const fs = require('fs')
const path = require('path')
const _ = require('lodash')
const Koa = require('koa')
const cors = require('@koa/cors')
const jwt = require('koa-jwt')
const bodyParser = require('koa-bodyparser')
const config = require('config')
const Grid = require('gridfs-stream')

const mongoose = require('./lib/mongoose')
const error = require('./lib/error')

// Some variables used in app

const app = new Koa()
let server = {}
let gfs = {}

// Functions to setup the app

function getFileDirectories(filename, filename2 = '') {
	const isDirectory = source => fs.lstatSync(source).isDirectory()
	const getDirectories = source => fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory)

	const dirs = getDirectories(path.join(__dirname, 'modules'))
	return dirs
		.filter(d => {
			const dirname = d.split(path.sep).pop()
			const file = path.join(`${d}`, `${dirname}.${filename}.js`)
			const file2 = path.join(`${d}`, `${dirname}.${filename}.${filename2}.js`)

			return fs.existsSync(file) || fs.existsSync(file2)
		})
}

function requireModules(filename, filename2 = '') {
	return getFileDirectories(filename, filename2)
		.map(d => {
			const dirname = d.split(path.sep).pop()
			const filepath = `/${dirname}.${filename}.js`
			const filepath2 = `/${dirname}.${filename}.${filename2}.js`

			const file = `./modules/${dirname}` + filepath
			const file2 = `./modules/${dirname}` + filepath2

			let type1 = {}
			let type2 = {}

			if (fs.existsSync(`${d}` + filepath)) {
				type1 = require(file)
			}

			if (fs.existsSync(`${d}` + filepath2)) {
				type2 = require(file2)
			}

			return {
				type1,
				type2
			}
		})
}

function connectDB(connection = undefined) {
	const conn = connection || config.db.connection

	return mongoose.connect(conn, {
		useMongoClient: true
	})
}

async function shutdown() {
	console.log('Shutting down...')

	try {
		await mongoose.disconnect()
		await server.close()

		console.log('Exit')
		return true
	} catch (err) {
		console.log(err)
		return false
	}
}

function setup() {
	app.use(cors())

	app.context.db = mongoose
	app.context.shutdown = shutdown

	// First argument is type1, second is type2
	const routers = requireModules('router', 'public')

	app.use(bodyParser())

	app.use(error)

	routers.forEach(r => {
		if (!_.isEmpty(r.type2)) {
			app.use(r.type2)
		}
	})

	app.use(jwt({
		secret: config.jwt.secret
	}))

	routers.forEach(r => {
		if (!_.isEmpty(r.type1)) {
			app.use(r.type1)
		}
	})

	return app
}

async function start(port = undefined, mongo = undefined) {
	try {
		await connectDB(mongo)
	} catch (err) {
		console.log('Could not connect to database!')
		return false
	}

	gfs = Grid(mongoose.connection.db, mongoose.mongo)
	app.context.gfs = gfs

	setup()

	const theport = port || config.port

	server = app.listen(theport)

	console.log('App started on port:', theport)

	return server
}

module.exports = {
	start,
	app,
	server,
	gfs,
	shutdown
}
