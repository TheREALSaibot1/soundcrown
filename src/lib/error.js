const error = require('koa-json-error')
const compose = require('koa-compose')
const _ = require('lodash')

const options = {
	// Avoid showing the stacktrace in 'production' env
	postFormat: (e, obj) => process.env.NODE_ENV === 'production' ? _.omit(obj, 'stack') : obj
}

module.exports = compose([error(options), async (ctx, next) => {
	try {
		await next()
	} catch (err) {
		// Handle your god damn errors here.
		// Check if error === "mongo died", or other "Unknown"/"fucked up" application state that requires you to MURDER THE DAMN SERVER.
		// then ctx.throw(500, "Something went wrong")

		if (err.message.startsWith('failed to reconnect')) {
			ctx.shutdown()
			ctx.throw(500, 'Error with mongoDB')
		}

		// If just a normal error like "not found":
		ctx.throw(err)
	}
}])
