const config = require('config')
const jwt = require('jsonwebtoken')

const users = require('../users/users.methods')
const UsersModel = require('../users/users.model')

const login = async ctx => {
	const user = await users.getUserByEmail(ctx.request.body.email, ['projects'])

	ctx.assert(user, 401, 'Login failed')

	const check = await UsersModel.checkPassword(ctx.request.body.password, user.password)

	ctx.assert(check, 401, 'Login failed')

	const token = jwt.sign({
		id: user._id.toString(),
		email: user.email,
		username: user.username,
		roles: user.roles
	}, config.jwt.secret)

	ctx.body = token
}

module.exports = {
	login
}
