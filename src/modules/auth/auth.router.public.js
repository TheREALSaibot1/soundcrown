const router = require('koa-joi-router')

const auth = require('./auth')

const authRouter = router()

const routes = [{
	method: 'post',
	path: '/login',
	handler: auth.login
}]

authRouter.route(routes)

authRouter.prefix('/auth')

module.exports = authRouter.middleware()
