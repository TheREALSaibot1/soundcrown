const health = async ctx => {
	ctx.body = 'App is healthy'
}

module.exports = {
	health
}
