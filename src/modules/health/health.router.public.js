const router = require('koa-joi-router')

const health = require('./health')

const healthRouter = router()

const routes = [{
	method: 'get',
	path: '/',
	handler: health.health
}]

healthRouter.route(routes)

healthRouter.prefix('/health')

module.exports = healthRouter.middleware()
