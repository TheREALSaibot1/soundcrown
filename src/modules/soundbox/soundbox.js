const fs = require('fs')
const path = require('path')
const Promise = require('bluebird')
const config = require('config')
const jwt = require('jsonwebtoken')
const _ = require('lodash')
const moment = require('moment')
const awaitWriteStream = require('await-stream-ready').write

const UsersModel = require('../users/users.model')
const SoundboxModel = require('./soundbox.model')

const create = async ctx => {
	const body = ctx.request.body
	const soundbox = body.soundbox
	const user = ctx.state.user

	const newsoundbox = await SoundboxModel.create({
		user: user.id,
		title: soundbox.title,
		description: soundbox.description,
		public: soundbox.public,
		icon: soundbox.icon
	})

	ctx.body = newsoundbox
}

const addCollaborators = async ctx => {
	const body = ctx.request.body
	const newcollabs = body.collaborators

	const sb = await SoundboxModel.findById(body.id).select('-icon')

	if (sb.user.toString() !== ctx.state.user.id) {
		ctx.throw(401, 'User not authorized')
	}

	const users = await UsersModel.find()
		.where('email')
		.in(newcollabs)
		.exec()
		.map(u => {
			return {
				id: u._id.toString(),
				username: u.username,
				email: u.email,
				icon: u.icon
			}
		})

	if (_.isEmpty(users)) {
		ctx.throw(400, 'Users do not exist')
	}

	const userIds = users.map(u => {
		return u.id
	})

	const sboxAuthor = userIds.find(u => {
		return u === sb.user.toString()
	})

	if (sboxAuthor) {
		ctx.throw(400, 'Owner cannot be added as collaborator to own soundbox')
	}

	const collabs = sb.collaborators.map(c => {
		return c.toString()
	})

	sb.collaborators = _.union(collabs, userIds)

	await sb.save()

	ctx.body = users
}

const removeCollaborators = async ctx => {
	const body = ctx.request.body

	const sb = await SoundboxModel.findById(body.id).select('-icon')

	if (sb.user.toString() !== ctx.state.user.id) {
		ctx.throw(401, 'User not authorized')
	}

	const removed = body.collaborators

	sb.collaborators = sb.collaborators.filter(c => {
		return removed.indexOf(c.toString()) === -1
	})

	ctx.body = await sb.save()
}

const leaveProject = async ctx => {
	const body = ctx.request.body

	const sb = await SoundboxModel.findById(body.id).select('-icon')

	const userId = ctx.state.user.id

	const user = sb.collaborators.find(c => {
		return c.toString() === userId
	})

	if (!user) {
		ctx.throw(401, 'User not authorized')
	}

	sb.collaborators = sb.collaborators.filter(c => {
		return c.toString() !== user.toString()
	})

	await sb.save()

	ctx.body = 'Left soundbox successfully'
}

const getProject = async ctx => {
	const	id = ctx.params.id

	const sb = await SoundboxModel.findById(id)
		.populate([{
			path: 'user',
			select: '_id email username icon'
		}, {
			path: 'collaborators',
			select: '_id email username icon'
		}, {
			path: 'soundfiles.author',
			select: 'email username'
		}, {
			path: 'releases.author',
			select: 'email username'
		}, {
			path: 'chat.user',
			select: 'email username'
		}])

	const userId = ctx.state.user.id

	const user = sb.collaborators.find(c => {
		return c._id.toString() === userId
	})

	if (!user && sb.user._id.toString() !== userId) {
		ctx.throw(401, 'User not authorized')
	}

	ctx.body = sb
}

const getProjects = async ctx => {
	const userId = ctx.state.user.id

	ctx.body = await SoundboxModel.find({
		$or: [{
			user: userId
		}, {
			collaborators: {
				$in: [userId]
			}
		}]
	})
	.populate([{
		path: 'user',
		select: 'email username'
	}, {
		path: 'releases.author',
		select: 'email username'
	}])
}

const addFile = async ctx => {
	const parts = ctx.request.parts
	let part
	let file
	let filePath

	const unlink = Promise.promisify(fs.unlink, {context: fs})

	try {
		while ((part = await parts)) {
			file = part
			filePath = path.join(__dirname, '../../assets/files/' + file.filename)
			// Do something with the incoming part stream
			const ws = fs.createWriteStream(filePath)
			const fileSaveLocal = file.pipe(ws)
			await awaitWriteStream(fileSaveLocal)

			const sb = await SoundboxModel.findById(parts.field.soundbox).select('-icon')

			const userId = ctx.state.user.id

			const user = sb.collaborators.find(c => {
				return c.toString() === userId
			})

			if (!user && sb.user.toString() !== userId) {
				await unlink(filePath)
				ctx.throw(401, 'User not authorized')
			}

			const fileName = parts.field.fileName
			const fileType = parts.field.fileType
			const category = parts.field.category
			const author = userId

			const writestream = ctx.gfs.createWriteStream({
				filename: fileName,
				mode: 'w',
				content_type: fileType
			})

			console.log('Saving file...')

			const result = fs.createReadStream(filePath).pipe(writestream)
			await awaitWriteStream(result)

			console.log('File saved!')

			const newfile = {
				title: fileName,
				file: result.id,
				category,
				author
			}

			sb.soundfiles.push(newfile)

			await unlink(filePath)

			await sb.save()

			ctx.body = await SoundboxModel.populate(sb, {
				path: 'soundfiles.author',
				select: '-_id email username'
			})
		}
	} catch (err) {
		if (fs.existsSync(filePath)) {
			await unlink(filePath)
		}

		ctx.throw(400, err)
	}
}

const addRelease = async ctx => {
	const parts = ctx.request.parts
	let part
	let file
	let filePath

	const unlink = Promise.promisify(fs.unlink, {context: fs})

	try {
		while ((part = await parts)) {
			file = part
			filePath = path.join(__dirname, '../../assets/files/' + file.filename)
			// Do something with the incoming part stream
			const ws = fs.createWriteStream(filePath)
			const fileSaveLocal = file.pipe(ws)
			await awaitWriteStream(fileSaveLocal)

			const sb = await SoundboxModel.findById(parts.field.soundbox).select('-icon')

			const userId = ctx.state.user.id

			const user = sb.collaborators.find(c => {
				return c.toString() === userId
			})

			if (!user && sb.user.toString() !== userId) {
				await unlink(filePath)
				ctx.throw(401, 'User not authorized')
			}

			const fileName = parts.field.fileName
			const fileType = parts.field.fileType
			const cover = parts.field.cover
			const author = userId

			const writestream = ctx.gfs.createWriteStream({
				filename: fileName,
				mode: 'w',
				content_type: fileType
			})

			console.log('Saving release...')

			const result = fs.createReadStream(filePath).pipe(writestream)
			await awaitWriteStream(result)

			console.log('Release saved!')

			const release = {
				createdAt: moment().toDate(),
				title: fileName,
				cover,
				file: result.id,
				author
			}

			sb.releases.push(release)

			await unlink(filePath)

			await sb.save()

			ctx.body = await SoundboxModel.populate(sb, {
				path: 'releases.author',
				select: '-_id email username'
			})
		}
	} catch (err) {
		if (fs.existsSync(filePath)) {
			await unlink(filePath)
		}

		ctx.throw(400, err)
	}
}

const getPublicReleaseFile = async ctx => {
	const params = ctx.params

	const sb = await SoundboxModel.findById(params.soundbox).select('-icon')

	const publicRelease = sb.releases.find(r => {
		return r.file.toString() === params.file && r.public
	})

	if (!publicRelease) {
		ctx.throw(404, 'No public release found')
	}

	const token = jwt.sign({
		expiresIn: '5m',
		soundbox: sb._id.toString(),
		file: params.file.toString()
	}, config.jwt.secret)

	ctx.body = token
}

const getFile = async ctx => {
	const params = ctx.params

	const sb = await SoundboxModel.findById(params.soundbox).select('-icon')

	const userId = ctx.state.user.id

	const user = sb.collaborators.find(c => {
		return c.toString() === userId
	})

	if (!user && sb.user.toString() !== userId) {
		ctx.throw(401, 'User not authorized')
	}

	const token = jwt.sign({
		expiresIn: '5m',
		soundbox: sb._id.toString(),
		file: params.file.toString()
	}, config.jwt.secret)

	ctx.body = token
}

const getFilePublic = async ctx => {
	const params = ctx.params
	const token = params.token

	const data = jwt.verify(token, config.jwt.secret) // Should hold soundbox and file ObjectId

	const findOne = Promise.promisify(ctx.gfs.findOne, {context: ctx.gfs})

	const fileData = await findOne({
		_id: data.file
	})

	const size = fileData.length

	ctx.set('Content-Type', fileData.contentType)
	ctx.set('Content-Length', size)

	ctx.body = ctx.gfs.createReadStream({
		_id: data.file
	})
}

const getFileData = async ctx => {
	const params = ctx.params

	const sb = await SoundboxModel.findById(params.soundbox)
		.select('-icon')
		.populate([{
			path: 'soundfiles.comments.user',
			select: 'email username'
		}, {
			path: 'soundfiles.author',
			select: 'email username icon'
		}])

	const userId = ctx.state.user.id

	const user = sb.collaborators.find(c => {
		return c.toString() === userId
	})

	if (!user && sb.user.toString() !== userId) {
		ctx.throw(401, 'User not authorized')
	}

	const file = sb.soundfiles.find(s => {
		return s.file.toString() === params.file
	})

	ctx.body = {
		sbox: sb.title,
		file
	}
}

const getReleaseData = async ctx => {
	const params = ctx.params

	const sb = await SoundboxModel.findById(params.soundbox)
		.select('-icon')
		.populate([{
			path: 'releases.comments.user',
			select: '_id email username'
		}, {
			path: 'releases.author',
			select: '_id email username icon'
		}])

	const userId = ctx.state.user.id

	const user = sb.collaborators.find(c => {
		return c.toString() === userId
	})

	const release = sb.releases.find(s => {
		return s.file.toString() === params.file
	})

	if (!release.public && !user && sb.user.toString() !== userId) {
		ctx.throw(401, 'User not authorized')
	}

	ctx.body = {
		sbox: sb.title,
		release
	}
}

/* Get more files

const getFiles = async ctx => {
	const params = ctx.params

	const sb = await SoundboxModel.findById(params.soundbox)

	if (sb.user !== ctx.state.user.id) {
		ctx.throw(401, 'User not authorized')
	}

	const files = await ctx.gfs.files.find({
		_id: {
			$in: sb.soundfiles
		}
	}).toArray()

	console.log("files:", files)

	const fuck = ctx.gfs.createReadStream({
		_id: files[0]._id.toString()
	})

	const loadedFiles = []

	files.forEach(f => {
		const file = ctx.gfs.createReadStream({
			_id: f._id.toString()
		})
		loadedFiles.push(file)
	})

	ctx.body = loadedFiles
} */

const update = async ctx => {
	const body = ctx.request.body

	const soundbox = {
		title: body.title || '',
		description: body.description || ''
	}

	const sb = await SoundboxModel.findById(body.id)

	if (sb.user.toString() !== ctx.state.user.id) {
		ctx.throw(401, 'User not authorized')
	}

	Object.keys(soundbox).forEach(s => {
		if (soundbox[s]) {
			sb.set({
				[s]: soundbox[s]
			})
		}
	})

	ctx.body = await sb.save()
}

const addChatMessage = async ctx => {
	const body = ctx.request.body

	const sb = await SoundboxModel.findById(body.id).select('-icon')

	const userId = ctx.state.user.id

	const user = sb.collaborators.find(c => {
		return c.toString() === userId
	})

	if (!user && sb.user.toString() !== userId) {
		ctx.throw(401, 'User not authorized')
	}

	const message = body.message
	message.date = moment(message.date).unix()
	sb.chat.push(message)

	ctx.body = await sb.save()
}

const addReleaseComment = async ctx => {
	const body = ctx.request.body

	const sb = await SoundboxModel.findById(body.id).select('-icon')

	const userId = ctx.state.user.id

	const user = sb.collaborators.find(c => {
		return c.toString() === userId
	})

	const releaseId = body.release

	const release = sb.releases.find(r => {
		return r._id.toString() === releaseId
	})

	if (!release.public && !user && sb.user.toString() !== userId) {
		ctx.throw(401, 'User not authorized')
	}

	const comment = body.message
	comment.date = moment(comment.date).unix()

	release.comments.push(comment)

	ctx.body = await sb.save()
}

const addSoundfileComment = async ctx => {
	const body = ctx.request.body

	const sb = await SoundboxModel.findById(body.id).select('-icon')

	const userId = ctx.state.user.id

	const user = sb.collaborators.find(c => {
		return c.toString() === userId
	})

	if (!user && sb.user.toString() !== userId) {
		ctx.throw(401, 'User not authorized')
	}

	const soundFileId = body.soundfile

	const soundfile = sb.soundfiles.find(s => {
		return s._id.toString() === soundFileId
	})

	const comment = body.message
	comment.date = moment(comment.date).unix()

	soundfile.comments.push(comment)

	ctx.body = await sb.save()
}

const getRecentReleases = async ctx => {
	const sb = await SoundboxModel.find({
		$and: [{
			'releases.createdAt': {
				$gt: moment().subtract(2, 'weeks').toDate()
			}
		}, {
			'releases.public': true
		}]
	})
	.select('releases')
	.populate({
		path: 'releases.author',
		select: 'email username'
	})

	const releases = sb.map(s => {
		return s.releases.filter(r => {
			return r.createdAt > moment().subtract(2, 'weeks').toDate() && r.public
		})
		.map(r => {
			return {
				sbId: s._id.toString(),
				release: r
			}
		})
	})

	const flatten = _.flatten(releases)

	ctx.body = flatten
}

const getPopularReleases = async ctx => {
	const sb = await SoundboxModel.aggregate([{
		$unwind: '$releases'
	}, {
		$unwind: '$releases.ratings'
	}, {
		$group: {
			_id: '$releases._id',
			avgRating: {
				$avg: '$releases.ratings.rating'
			}
		}
	}, {
		$group: {
			_id: null,
			ratings: {
				$push: '$$ROOT'
			},
			avgRating: {
				$avg: '$avgRating'
			}
		}
	}, {
		$project: {
			ratings: {
				$filter: {
					input: '$ratings',
					as: 'rating',
					cond: {
						$gte: [
							'$$rating.avgRating',
							'$avgRating'
						]
					}
				}
			},
			avgRating: '$avgRating'
		}
	}])

	if (!sb[0]) {
		ctx.body = []
		return
	}

	const releases = sb[0].ratings.map(s => {
		return s._id.toString()
	})

	const popular = await SoundboxModel.find()
		.where('releases._id').in(releases)
		.select('releases')
		.populate({
			path: 'releases.author',
			select: 'email username'
		})
		.exec()
		.map(s => {
			return s.releases.filter(r => {
				return (releases.indexOf(r._id.toString()) !== -1) && r.public
			})
			.map(r => {
				return {
					sbId: s._id.toString(),
					release: r
				}
			})
		})

	const flatten = _.flatten(popular)

	ctx.body = flatten
}

const addReleaseRating = async ctx => {
	const body = ctx.request.body

	const sb = await SoundboxModel.findById(body.id).select('-icon')

	const userId = ctx.state.user.id

	const user = sb.collaborators.find(c => {
		return c.toString() === userId
	})

	const release = sb.releases.find(r => {
		return r._id.toString() === body.release
	})

	if ((!release.public && !user && sb.user.toString() !== userId) || release.author.toString() === userId) {
		ctx.throw(401, 'User not authorized')
	}

	const ratingFromUser = release.ratings.find(r => {
		return r.user.toString() === userId
	})

	if (ratingFromUser) {
		ratingFromUser.rating = body.rating
	} else {
		const newRating = {
			user: userId,
			rating: body.rating
		}

		release.ratings.push(newRating)
	}

	ctx.body = await sb.save()
}

const getUserReleases = async ctx => {
	const user = ctx.params.user

	const sb = await SoundboxModel.find({
		'releases.author': user
	})

	const releases = sb.map(s => {
		return s.releases.filter(r => {
			return r.author.toString() === user && r.public
		})
		.map(r => {
			return {
				sbId: s._id.toString(),
				release: r
			}
		})
	})

	const flatten = _.flatten(releases)

	ctx.body = flatten
}

const publishRelease = async ctx => {
	const body = ctx.request.body

	const sb = await SoundboxModel.findById(body.id)

	const userId = ctx.state.user.id

	const release = sb.releases.find(r => {
		return r._id.toString() === body.release
	})

	if (release.author.toString() !== userId) {
		ctx.throw(401, 'User not authorized')
	}

	release.public = true

	await sb.save()

	ctx.body = release
}

module.exports = {
	create,
	addCollaborators,
	removeCollaborators,
	leaveProject,
	getProject,
	getProjects,
	addFile,
	addRelease,
	getFile,
	getPublicReleaseFile,
	getFilePublic,
	getFileData,
	getReleaseData,
	update,
	addChatMessage,
	addReleaseComment,
	addSoundfileComment,
	getRecentReleases,
	getPopularReleases,
	addReleaseRating,
	getUserReleases,
	publishRelease
}
