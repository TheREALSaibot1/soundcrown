const mongoose = require('mongoose')
const SoundboxSchema = require('./soundbox.schema')

module.exports = mongoose.model('Soundbox', SoundboxSchema)
