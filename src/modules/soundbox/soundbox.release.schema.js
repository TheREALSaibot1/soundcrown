const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

const ReleaseSchema = new Schema({
	createdAt: {
		type: Date,
		required: true
	},
	title: {
		type: String,
		required: true
	},
	cover: String,
	author: {
		type: ObjectId,
		ref: 'Users',
		required: true
	},
	file: {
		type: ObjectId,
		required: true
	},
	comments: [{
		_id: false,
		user: {
			type: ObjectId,
			ref: 'Users',
			required: true
		},
		text: {
			type: String,
			required: true
		},
		date: {
			type: Number,
			required: true
		}
	}],
	ratings: [{
		_id: false,
		user: {
			type: ObjectId,
			ref: 'Users',
			required: true
		},
		rating: {
			type: Number,
			required: true
		}
	}],
	public: {
		type: Boolean,
		default: false
	}
})

module.exports = ReleaseSchema
