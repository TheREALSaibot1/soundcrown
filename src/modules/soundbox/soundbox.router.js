const router = require('koa-joi-router')

const roles = require('koa-jwt-roles')

const soundbox = require('./soundbox')

const soundboxRouter = router()

const routes = [{
	method: 'post',
	path: '/',
	handler: soundbox.create
}, {
	method: 'put',
	path: '/',
	handler: soundbox.update
}, {
	method: 'put',
	path: '/collaborators',
	handler: soundbox.addCollaborators
}, {
	method: 'put',
	path: '/collaborators/remove',
	handler: soundbox.removeCollaborators
}, {
	method: 'put',
	path: '/leave',
	handler: soundbox.leaveProject
}, {
	method: 'get',
	path: '/:id',
	handler: soundbox.getProject
}, {
	method: 'get',
	path: '/',
	handler: soundbox.getProjects
}, {
	method: 'post',
	path: '/file',
	validate: {
		type: 'multipart'
	},
	handler: soundbox.addFile
}, {
	method: 'post',
	path: '/release',
	validate: {
		type: 'multipart'
	},
	handler: soundbox.addRelease
}, {
	method: 'get',
	path: '/:soundbox/file/:file',
	handler: soundbox.getFile
}, {
	method: 'get',
	path: '/:soundbox/file/:file/data',
	handler: soundbox.getFileData
}, {
	method: 'get',
	path: '/:soundbox/release/:file/data',
	handler: soundbox.getReleaseData
}, {
	method: 'get',
	path: '/:soundbox/release/:file/public',
	handler: soundbox.getPublicReleaseFile
}, {
	method: 'post',
	path: '/chat',
	handler: soundbox.addChatMessage
}, {
	method: 'get',
	path: '/release/recent',
	handler: soundbox.getRecentReleases
}, {
	method: 'get',
	path: '/release/popular',
	handler: soundbox.getPopularReleases
}, {
	method: 'post',
	path: '/release/comment',
	handler: soundbox.addReleaseComment
}, {
	method: 'post',
	path: '/file/comment',
	handler: soundbox.addSoundfileComment
}, {
	method: 'post',
	path: '/release/rating',
	handler: soundbox.addReleaseRating
}, {
	method: 'get',
	path: '/release/user/:user',
	handler: soundbox.getUserReleases
}, {
	method: 'put',
	path: '/release/publish',
	handler: soundbox.publishRelease
}]

soundboxRouter.route(routes)

soundboxRouter.prefix('/soundbox')

soundboxRouter.use(roles('user'))

module.exports = soundboxRouter.middleware()
