const router = require('koa-joi-router')

const soundbox = require('./soundbox')

const soundboxRouter = router()

const routes = [{
	method: 'get',
	path: '/token/:token',
	handler: soundbox.getFilePublic
}]

soundboxRouter.route(routes)

soundboxRouter.prefix('/soundbox')

module.exports = soundboxRouter.middleware()
