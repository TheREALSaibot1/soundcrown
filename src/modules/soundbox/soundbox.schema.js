const mongoose = require('mongoose')
const ReleaseSchema = require('./soundbox.release.schema')
const SoundfileSchema = require('./soundbox.soundfile.schema')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

const SoundboxSchema = new Schema({
	user: {
		type: ObjectId,
		ref: 'Users',
		required: true
	},
	title: {
		type: String,
		required: true
	},
	public: {
		type: Boolean,
		default: false,
		required: true
	},
	icon: String,
	description: {
		type: String,
		required: true
	},
	collaborators: [{
		type: ObjectId,
		ref: 'Users'
	}],
	soundfiles: [SoundfileSchema],
	releases: [ReleaseSchema],
	chat: [{
		_id: false,
		user: {
			type: ObjectId,
			ref: 'Users',
			required: true
		},
		text: {
			type: String,
			required: true
		},
		date: {
			type: Number,
			required: true
		}
	}]
})

module.exports = SoundboxSchema
