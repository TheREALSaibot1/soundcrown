const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

const ReleaseSchema = new Schema({
	title: {
		type: String,
		required: true
	},
	author: {
		type: ObjectId,
		ref: 'Users',
		required: true
	},
	file: {
		type: ObjectId,
		required: true
	},
	category: {
		type: String,
		enum: ['vocals', 'beats', 'mix', 'instrumental'],
		required: true
	},
	comments: [{
		_id: false,
		user: {
			type: ObjectId,
			ref: 'Users',
			required: true
		},
		text: {
			type: String,
			required: true
		},
		date: {
			type: Number,
			required: true
		}
	}]
})

module.exports = ReleaseSchema
