const UsersModel = require('./users.model')
const users = require('./users.methods')

const create = async ctx => {
	const user = ctx.request.body
	const pw = await UsersModel.password(user.password)

	const newuser = await UsersModel.create({
		email: user.email,
		password: pw,
		username: user.username,
		icon: user.icon,
		roles: [
			'user'
		]
	})

	ctx.body = newuser
}

const update = async ctx => {
	const body = ctx.request.body
	const id = body.id

	const user = await users.getUserById(id, ['password', 'roles'])

	if (user._id.toString() !== ctx.state.user.id) {
		ctx.throw(401, 'User not authorized')
	}

	const updatedUser = {
		username: body.username || '',
		bio: body.bio || '',
		icon: body.icon || ''
	}

	Object.keys(updatedUser).forEach(k => {
		if (updatedUser[k]) {
			user.set({
				[k]: updatedUser[k]
			})
		}
	})

	ctx.body = await user.save()
}

const getUserById = async ctx => {
	const id = ctx.params.id
	const user = await users.getUserById(id, ['password', 'roles'])

	ctx.body = user
}

const getUserByEmail = async ctx => {
	const email = ctx.params.email
	const user = await users.getUserByEmail(email, ['password', 'roles'])

	ctx.body = user
}

module.exports = {
	create,
	update,
	getUserById,
	getUserByEmail
}
