const Users = require('./users.model')

async function getUserByEmail(email, excludes = []) {
	const select = excludes.map(e => {
		return '-' + e
	}).join(' ')

	return Users.findOne({
		email
	})
	.select(select)
}

async function getUserById(id, excludes = []) {
	const select = excludes.map(e => {
		return '-' + e
	}).join(' ')

	return Users.findById(id)
	.select(select)
}

module.exports = {
	getUserByEmail,
	getUserById
}
