const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const UsersSchema = require('./users.schema')

UsersSchema.statics.password = function (pw) {
	return bcrypt.hash(pw, 10)
}

UsersSchema.statics.checkPassword = function (pw, hash) {
	return bcrypt.compare(pw, hash)
}

module.exports = mongoose.model('Users', UsersSchema)
