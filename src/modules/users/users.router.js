const router = require('koa-joi-router')

const roles = require('koa-jwt-roles')

const users = require('./users')

const usersRouter = router()

const routes = [{
	method: 'get',
	path: '/:id',
	handler: users.getUserById
}, {
	method: 'get',
	path: '/email/:email',
	handler: users.getUserByEmail
}, {
	method: 'put',
	path: '/',
	handler: users.update
}]

usersRouter.route(routes)

usersRouter.prefix('/user')

usersRouter.use(roles('user'))

module.exports = usersRouter.middleware()
