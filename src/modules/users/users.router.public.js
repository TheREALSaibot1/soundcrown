const router = require('koa-joi-router')

const users = require('./users')

const usersRouter = router()

const routes = [{
	method: 'post',
	path: '/',
	handler: users.create
}]

usersRouter.route(routes)

usersRouter.prefix('/user')

module.exports = usersRouter.middleware()
