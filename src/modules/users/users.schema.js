const mongoose = require('mongoose')

const Schema = mongoose.Schema

const UsersSchema = new Schema({
	email: {
		unique: true,
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	username: {
		type: String,
		required: true
	},
	bio: String,
	icon: String,
	roles: [{
		type: String,
		required: true
	}]
})

module.exports = UsersSchema
