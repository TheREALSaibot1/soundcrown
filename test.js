import path from 'path'
import fs from 'fs'
import test from 'ava'
import jwt from 'jsonwebtoken'
import assertRequest from 'assert-request'
import Grid from 'gridfs-stream'
import config from 'config'
import mongoose from './src/lib/mongoose'
import app from './src/app'
import Users from './src/modules/users/users.model'
import Soundbox from './src/modules/soundbox/soundbox.model'

let request = {}
let testuser = {}
let testuser2 = {}
let soundbox = {}
let soundbox2 = {}

let token = {}
let token2 = {}

let gfs = {}
let theapp = {}

test.before(async () => {
	theapp = await app.start(9001, 'mongodb://localhost/soundcrowntest')

	request = assertRequest(theapp)
	gfs = Grid(mongoose.connection.db, mongoose.mongo)

	await mongoose.connection.db.dropDatabase()
})

test.before(async () => {
	const pw = await Users.password('nicu')
	const pw2 = await Users.password('nicu2')

	testuser = await Users.create({
		email: 'testguy@test.com',
		password: pw,
		name: {
			first: 'Test',
			last: 'Guy'
		},
		username: 'testguy',
		roles: [
			'user'
		],
		projects: []
	})

	testuser2 = await Users.create({
		email: 'testguy2@test.com',
		password: pw2,
		name: {
			first: 'Test2',
			last: 'Guy2'
		},
		username: 'testguy2',
		roles: [
			'user'
		],
		projects: []
	})

	let writestream = gfs.createWriteStream({
		filename: 'testfile.mp3',
		mode: 'w',
		content_type: 'audio/mpeg'
	})

	const result = fs.createReadStream(path.join(__dirname, '/src/assets/files/sourcefile.mp3')).pipe(writestream)

	writestream = gfs.createWriteStream({
		filename: 'testfile2.mp3',
		mode: 'w',
		content_type: 'audio/mpeg'
	})

	const result2 = fs.createReadStream(path.join(__dirname, '/src/assets/files/sourcefile.mp3')).pipe(writestream)

	soundbox = await Soundbox.create({
		user: testuser._id,
		title: 'Test soundbox',
		description: 'A test soundbox',
		collaborators: [
			testuser2._id
		],
		soundfiles: [{
			title: 'Soundfile #1',
			author: testuser._id,
			category: 'instrumental',
			comments: [],
			file: result.id
		}],
		song: mongoose.Schema.Types.ObjectId('')
	})

	soundbox2 = await Soundbox.create({
		user: testuser2._id,
		title: 'Test soundbox2',
		description: 'A test soundbox no. 2',
		collaborators: [
			testuser._id
		],
		soundfiles: [{
			title: 'Soundfile #2',
			author: testuser._id,
			category: 'mix',
			comments: [],
			file: result2.id
		}],
		song: mongoose.Schema.Types.ObjectId('')
	})

	token = jwt.sign({
		id: testuser._id,
		email: testuser.email,
		roles: [
			'user'
		]
	}, config.jwt.secret)

	token2 = jwt.sign({
		id: testuser2._id,
		email: testuser2.email,
		roles: [
			'user'
		]
	}, config.jwt.secret)
})

test('create user', () => {
	const user = {
		email: 'imafucking@idiot.com',
		password: 'password',
		username: 'immistermeeseeks',
		name: {
			first: 'Mee',
			last: 'Seeks'
		}
	}

	const options = {
		method: 'POST',
		body: user,
		json: true
	}

	return request('/user', options)
		.status(200)
		.assert(res => {
			return res.body.email === 'imafucking@idiot.com'
		})
})

test('login', () => {
	const options = {
		method: 'POST',
		body: {
			email: testuser.email,
			password: 'nicu'
		},
		json: true
	}

	return request('/auth/login', options)
		.status(200)
		.assert(res => {
			const token = jwt.decode(res.body)
			return token.email === testuser.email
		})
})

test('get user by id', () => {
	const options = {
		method: 'GET',
		headers: {
			authorization: 'Bearer ' + token2
		},
		json: true
	}

	return request(`/user/${testuser2._id}`, options)
		.status(200)
		.assert(res => {
			return res.body._id.toString() === testuser2._id.toString()
		})
})

test('get user by id without token', () => {
	const options = {
		method: 'GET',
		json: true
	}

	return request(`/user/${testuser2._id}`, options)
		.status(401)
})

test('get user by email', () => {
	const options = {
		method: 'GET',
		headers: {
			authorization: 'Bearer ' + token2
		},
		json: true
	}

	return request(`/user/email/${testuser2.email}`, options)
		.status(200)
		.assert(res => {
			return res.body.email === testuser2.email
		})
})

test('get user by email without token', () => {
	const options = {
		method: 'GET',
		json: true
	}

	return request(`/user/email/${testuser2.email}`, options)
		.status(401)
})

test('create soundbox', () => {
	const body = {
		user: {
			id: testuser.id
		},
		soundbox: {
			title: 'Rap god ft. Lil-Y',
			description: 'This is indeed a soundbox'
		}
	}

	const options = {
		method: 'POST',
		headers: {
			authorization: 'Bearer ' + token
		},
		body,
		json: true
	}

	return request('/soundbox', options)
		.status(200)
		.assert(res => {
			return res.body.title === 'Rap god ft. Lil-Y'
		})
})

test('edit soundbox', () => {
	const body = {
		id: soundbox._id,
		title: 'edited soundbox title',
		description: 'edited soundbox description'
	}

	const options = {
		method: 'PUT',
		headers: {
			authorization: 'Bearer ' + token
		},
		body,
		json: true
	}

	return request('/soundbox', options)
		.status(200)
		.assert(res => {
			const title = res.body.title === 'edited soundbox title'
			const description = res.body.description === 'edited soundbox description'
			return title && description
		})
})

test('edit soundbox 2', () => {
	const body = {
		id: soundbox2._id,
		title: '',
		description: 'edited soundbox description'
	}

	const options = {
		method: 'PUT',
		headers: {
			authorization: 'Bearer ' + token2
		},
		body,
		json: true
	}

	return request('/soundbox', options)
		.status(200)
		.assert(res => {
			const title = res.body.title === soundbox2.title
			const description = res.body.description === 'edited soundbox description'
			return title && description
		})
})

test('edit soundbox 3', () => {
	const body = {
		id: soundbox._id,
		title: 'edited soundbox title',
		description: ''
	}

	const options = {
		method: 'PUT',
		headers: {
			authorization: 'Bearer ' + token
		},
		body,
		json: true
	}

	return request('/soundbox', options)
		.status(200)
		.assert(res => {
			const title = res.body.title === 'edited soundbox title'
			const description = res.body.description === soundbox.description
			return title && description
		})
})

test('edit soundbox with another user', () => {
	const body = {
		id: soundbox._id,
		title: 'edited soundbox title',
		description: 'edited soundbox description'
	}

	const options = {
		method: 'PUT',
		headers: {
			authorization: 'Bearer ' + token2	// Different user from the creator
		},
		body,
		json: true
	}

	return request('/soundbox', options)
		.status(401)
})

test('get file', () => {
	const options = {
		method: 'GET',
		headers: {
			authorization: 'Bearer ' + token	// Different user from the creator
		},
		json: true
	}

	return request(`/soundbox/${soundbox._id}/file/${soundbox.soundfiles[0]}`, options)
		.status(200)
})

/* Add file implement */

test('remove collaborators', () => {
	const body = {
		id: soundbox._id,
		collaborators: [
			testuser2._id
		]
	}

	const options = {
		method: 'PUT',
		body,
		headers: {
			authorization: 'Bearer ' + token	// Different user from the creator
		},
		json: true
	}

	return request(`/soundbox/collaborators/remove`, options)
		.status(200)
		.assert(res => {
			return res.body.collaborators.length === 0
		})
})
